import {Publisher} from './helpers/Publisher.js';
import {TestController} from './components/Test/TestController.js';

const publisher = new Publisher();

const testComponent = new TestController(publisher.methods);
