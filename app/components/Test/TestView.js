export class TestView {
    constructor(){
        this.rootBlock = document.getElementById('root');
    }

    render(data) {
        this.rootBlock.innerHTML = data;
    }
}
