export class TestModel{
    constructor(){
        this.title = 'Hello world!';
    }

    get pageTitle() {
        return this.title;
    }
}
